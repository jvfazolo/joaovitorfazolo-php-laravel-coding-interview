<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostClientBookingRequest;
use App\Models\Booking;
use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ClientBookingController extends Controller
{
    /**
     * Get client's bookings.
     *
     * @param Client $client
     *
     * @return Collection
     */
    public function index(Client $client): Collection
    {
        return $client->bookings;
    }

    /**
     * Get a booking.
     *
     * @param Booking $booking
     *
     * @return Booking
     */
    public function show(Booking $booking): Booking
    {
        return $booking;
    }

    public function store(PostClientBookingRequest $request, Client $client)
    {
        if (!count($client->dogs)) {
            return response()->json(['cant book it'], 422);
        }
        $avgAge = $client->dogs->avg('age');
        $requestData = $request->all();
        if ($avgAge < 10) {
            $requestData['price'] = $requestData['price'] * 0.9;
        }

        $booking = new Booking($requestData);
        $client->bookings()->save($booking);

        return response()->json($booking, 201);
    }
}
